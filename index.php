<?php
// Парсер сайта hata.by
// Берет предложения по продаже
// Возвращает картинки для каждого объявления в отдельной папке с номером по порядку
// И сериализованный массив с данными объявлений в файле arr_ad.txt

set_time_limit(0);
ini_set('memory_limit', '1024m');
include 'simple_html_dom.php';

function is_in_str($str, $substr) {
    $result = strpos ($str, $substr);
    if ($result === FALSE)
        return false;
    else
        return true;
}

function object2file($value, $filename) {
    $str_value = serialize($value);

    $f = fopen($filename, 'w');
    fwrite($f, $str_value);
    fclose($f);
}

function object_from_file($filename)
{
    $file = file_get_contents($filename);
    $value = unserialize($file);
    return $value;
}

function dlPage($href) {

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_URL, $href);
    curl_setopt($curl, CURLOPT_REFERER, $href);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.4 (KHTML, like Gecko) Chrome/5.0.375.125 Safari/533.4");
    $str = curl_exec($curl);
    curl_close($curl);

    // Create a DOM object
    $dom = new simple_html_dom();
    // Load HTML from a string
    $dom->load($str);

    return $dom;
}

$countPage = 60;
//$countPage = 1; // для мозырь

$numbAdd = 1; // номер объявления
// идем по всем страницам с объявлениями
for($i = 1; $i <= $countPage; $i++) {

    $pageCardAd = dlPage('http://www.hata.by/search/~s_what=flat~s_do=sale~filter_ckod=%D0%91%D0%B5%D0%BB%D0%B0%D1%80%D1%83%D1%81%D1%8C~currency=840~search_type=big/page/'.$i.'/'); // все города
    //$pageCardAd = dlPage('http://www.hata.by/search/~s_what=flat~s_do=sale~ckod=3415000000~filter_ckod=%D0%B3.%20%D0%9C%D0%BE%D0%B7%D1%8B%D1%80%D1%8C~ctype=ckod~currency=840~search_type=small/page/'.$i.'/'); // мозырь
    //$pageCardAd = dlPage('http://www.hata.by/search/~s_what=flat~s_do=sale~ckod=3401000000~filter_ckod=%D0%92%20%D0%93%D0%BE%D0%BC%D0%B5%D0%BB%D0%B5~ctype=ckod~currency=840~search_type=small/page/'.$i.'/'); // гомель

    $cardAd = $pageCardAd->find('.showcase-small .showcase-small-item');

    foreach($cardAd as $ii => $cardItem) {

        // сбрасываем переменные
        $nameAd = null;
        $priceAd = null;
        $rooms_count = null;
        $square_general = null;
        $square_living = null;
        $square_kitchen = null;
        $floor = null;
        $floor_count = null;
        $city_full = null;
        $area = null;
        $micro_area = null;
        $street_full = null;
        $prospectus_full = null;
        $lane_full = null;
        $quay_full = null;
        $boulevard_full = null;
        $highway_full = null;
        $house = null;
        $type_house = null;
        $loggia = null;
        $year_construction = null;
        $repair = null;
        $toilet = null;
        $furniture = null;
        $arrPhones = null;
        $arrImg = null;
        $description = null;

        $linkAd = $cardItem->find('.showcase-title a', 0)->href; // ссылка на страницу с объявлением

        $pageAd = dlPage($linkAd); // на странице с объявлением

        $nameAd = trim(preg_replace('/\(.*?\)/', '', $pageAd->find('.card h1', 0)->plaintext)); // ! название объявления !

        $priceAd = trim(preg_replace('/([$ &nbsp;])/', '', $pageAd->find('.card .price', 0)->plaintext)); // ! цена !


        $strAddress = $pageAd->find('.card .open-map span', 0)->plaintext; // строка с адресом (разная)

        if(is_in_str($strAddress, ',') == true){
            $arrAddress = explode(',', $strAddress);

            if(mb_strrpos($arrAddress[0], 'г.') > 2) continue; // если город не в начале, пропускаем объявление !!!!

            $city_full = trim(str_replace('г.','', $arrAddress[0])); // ! город !

            if ($arrAddress[1]){
                $partAddress = explode('д.', $arrAddress[1]); // может и не быть

                // ! улица, дом, корп !
                if($partAddress[0]){
                    if(is_in_str($partAddress[0], 'ул.') == true) $street_full = trim(str_replace('ул.','', $partAddress[0])); // !
                    if(is_in_str($partAddress[0], 'пр.') == true) $prospectus_full = trim(str_replace('пр.','', $partAddress[0])); // !
                    if(is_in_str($partAddress[0], 'пер.') == true) $lane_full = trim(str_replace('пер.','', $partAddress[0])); // !
                    if(is_in_str($partAddress[0], 'наб.') == true) $quay_full = trim(str_replace('наб.','', $partAddress[0])); // !
                    if(is_in_str($partAddress[0], 'б-р') == true) $boulevard_full = trim(str_replace('б-р','', $partAddress[0])); // !
                    if(is_in_str($partAddress[0], 'шоссе') == true) $highway_full = trim(str_replace('шоссе','', $partAddress[0])); // !
                }
                if($partAddress[1]){
                    if(is_in_str($partAddress[1], 'к.') == true) {
                        $house = trim(str_replace('к.','/', $partAddress[1])); // !
                    } else {
                        $house = trim($partAddress[1]);
                    }
                } else {
                    $house = null;
                }
            }
        } else {
            $city_full = trim(str_replace('г.','', $strAddress)); // ! город !
        }



        $arrPhones = array(); // ! телефоны !
        foreach($pageAd->find('.card .phone') as $iii => $phones){
            if(filter_var(trim($phones->plaintext), FILTER_VALIDATE_EMAIL)) {
                continue;
            }
            if(preg_replace('/([^-0-9()+])/', '', $phones->plaintext)){
                $arrPhones[$iii] = trim(preg_replace('/([^-0-9()+])/', '', $phones->plaintext));
            } else {
                continue;
            }
        }

        // параметры квартиры
        if($pageAd->find('.card .card-params tr')){
            foreach($pageAd->find('.card .card-params tr') as $params){
                $paramName = $params->find('.name', 0)->plaintext;
                $paramValue = $params->find('.value', 0)->plaintext;

                if(trim($paramName) == 'Район') $area = trim(str_replace('район','',$paramValue)); // ! район !
                if(trim($paramName) == 'Микрорайон') $micro_area = trim($paramValue); // ! микрорайон !
                if(trim($paramName) == 'Этаж / Этажность') {
                    $floorArr = explode('/', $paramValue);
                    $floor = trim($floorArr[0]); // ! этаж !
                    $floor_count = trim($floorArr[1]); // ! этажность !
                }
                if(trim($paramName) == 'Комнат / Раздельных') {
                    $roomsArr = explode('/', $paramValue);
                    $rooms_count = trim($roomsArr[0]); // ! количество комнат !
                }
                if(trim($paramName) == 'Общая площадь') $square_general = trim(str_replace('м²','', $paramValue)); // ! общая площадь !
                if(trim($paramName) == 'Жилая площадь') $square_living = trim(str_replace('м²','', $paramValue));  // ! жилая площадь !
                if(trim($paramName) == 'Площадь кухни') $square_kitchen = trim(str_replace('м²','', $paramValue));  // ! площадь кухни !
                if(trim($paramName) == 'Балкон / Лоджия') $loggia = trim($paramValue);  // ! балкон / лоджия !
                if(trim($paramName) == 'Тип дома') $type_house = trim($paramValue);  // ! тип дома !
                if(trim($paramName) == 'Год постройки') $year_construction = trim($paramValue);  // ! год постройки !
                if(trim($paramName) == 'Ремонт') $repair = trim($paramValue);  // ! ремонт !
                if(trim($paramName) == 'Санузел') $toilet = trim($paramValue);  // ! санузел !
                if(trim($paramName) == 'Мебель') $furniture = trim($paramValue);  // ! мебель !

            }
        }

        $description = $pageAd->find('.card .card-descr', 0)->children(1)->plaintext; // ! описание !

        if(is_in_str($pageAd->find('.card .card-images', 0)->children(0)->find('a img', 0)->src, 'noimage') != true) {

            $linkImages = $pageAd->find('.card .card-images', 0)->children(0)->find('a', 0)->href; // ссылка на картинки

            $pageImg = dlPage($linkImages); // на странице с картинками

            $arrImg = array(); // ! картинки !
            foreach($pageImg->find('.carousel-stage li') as $i2 => $item) {

                $linkImg = $item->find('img', 0)->src; // ссылка на картинку

                $filepath = '/images/'.$numbAdd.'/';

                mkdir($_SERVER['DOCUMENT_ROOT'].$filepath, 0777, true);

                $img_line = getimagesize ($linkImg);
                if ($img_line[2] == IMAGETYPE_JPEG) {
                    $type = 'jpg';
                }
                if ($img_line[2] == IMAGETYPE_PNG) {
                    $type = 'png';
                }
                if ($img_line[2] == IMAGETYPE_GIF) {
                    $type = 'gif';
                }

                $s = file_get_contents($linkImg);

                file_put_contents($_SERVER['DOCUMENT_ROOT'].$filepath.$i2.'.'.$type, $s);

                $arrImg[$i2] = $filepath.$i2.'.'.$type;

                if($i2 == 10) break;
            }

        } else {
            $arrImg = null;
        }

        $arrAd[$numbAdd] = array(
            'name' => $nameAd,
            'price' => $priceAd,
            'rooms_count' => $rooms_count,
            'square_general' => $square_general,
            'square_living' => $square_living,
            'square_kitchen' => $square_kitchen,
            'floor' => $floor,
            'floor_count' => $floor_count,
            //'region_full' => $region_full,
            'city_full' => $city_full,
            'area' => $area,
            'micro_area' => $micro_area,
            'street_full' => $street_full,
            'prospectus_full' => $prospectus_full,
            'lane_full' => $lane_full,
            'quay_full' => $quay_full,
            'boulevard_full' => $boulevard_full,
            'highway_full' => $highway_full,
            'house' => $house,
            'house_type' => $type_house,
            'loggia' => $loggia,
            'year_construction' => $year_construction,
            'repair' => $repair,
            'toilet' => $toilet,
            'furniture' => $furniture,
            'phones' => $arrPhones,
            'images' => $arrImg,
            'description' => $description,
        );

        object2file($arrAd, 'arr_ad.txt');

        $numbAdd++;

        //if($ii == 2) break; // количество объявлений на странице
    }

    //if($i == 1) break; // количество страниц с объявлениями
}

//print_r(object_from_file('arr_ad.txt'));